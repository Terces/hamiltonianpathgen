# HamiltonianPathGen.gd implements a method for Monte Carlo sampling of 
# Hamiltonian paths. Initial implementation in JavaScript by Nathan Clisby
# on the website: http://clisby.net/projects/hamiltonian_path/
#
# Copyright (C) 2012, 2018, 2019 Tobias L.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# See https://www.gnu.org/licenses/ for details of the GNU General
# Public License. 

extends Node

# default settings, unless changed
var settings = {
	"xmax" : 10,
	"ymax" : 10,
	"start" : Vector2(0, 0),
	"end" : Vector2(10, 10),
	"fill_ratio_min" : 0.5,
	"fill_ratio_max" : 1.0,
	"strict_limit" : 0,
	"avoid" : []
}

# helping variables
var cardinals = [Vector2(1, 0), Vector2(-1, 0), Vector2(0, 1), Vector2(0, -1)]

func generate_hamiltonian_path():
	var path = [settings.start]
	# loop as long as we're not at the desired end or not have enough points
	var limit_min = int((settings.xmax+1) * (settings.ymax+1) * settings.fill_ratio_min) - settings.avoid.size()
	var limit_max = int((settings.xmax+1) * (settings.ymax+1) * settings.fill_ratio_max) - settings.avoid.size()
	while path.size() < limit_min or path.size() > limit_max or path.back() != settings.end:
		if path.size() > limit_max:
			path.clear()
			path = [settings.start]
		path = backbite(path)
	return path

func generate_hamiltonian_path_strict():
	var path = [settings.start]
	while path.size() != settings.strict_limit or path.back() != settings.end:
		if path.size() > settings.strict_limit:
			path.clear()
			path = [settings.start]
		path = backbite(path)
	return path

func backbite(path):
	randomize()
	var step = cardinals[randi() % 4]
	var neighbour = path.back() + step
	if settings.avoid.has(neighbour):
		return path
	if not inSublattice(neighbour):
		return path
	# check if the new point is already in the created path
	if path.has(neighbour):
		# if point is in path, reverse entries from the crossing point to the current end point.
		# This should prevent running into dead ends.
		path = reverseArrayPart(path.find(neighbour)+1 , path.size()-1, path)
		return path
	# if the point is a new point, add it to the path
	path.append(neighbour)
	return path

func inSublattice(point):
	# check if the point is inside the overall grid
	if point.x < 0 or point.x > settings.xmax:
		return false
	if point.y < 0 or point.y > settings.ymax:
		return false
	return true

func reverseArrayPart(start, end, array):
	# only run through half the loop; First half affects second half and would the other way around
	var loop_size = ((end - start) + 1) / 2
	for index in loop_size:
		var temp = array[start + index]
		array[start + index] = array[end - index]
		array[end - index] = temp
	return array