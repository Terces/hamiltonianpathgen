extends Node2D

var HamPathGen = preload("res://HamiltonianPathGen.gd").new()
var separation = 1

func _ready():
	HamPathGen.settings = {
		"xmax" : 10,
		"ymax" : 10,
		"start" : Vector2(0, 0),
		"end" : Vector2(10, 10),
		"fill_ratio_min" : 0.7,
		"fill_ratio_max" : 1.0,
		"strict_limit" : $CanvasLayer/Controls/mainCont/strictFill.value,
		"avoid" : []
	}
	var path = HamPathGen.generate_hamiltonian_path()
	for c in path.size():
		if c < path.size()-1:
			var vector = path[c+1] - path[c]
			var source_cell = path[c] * (separation + 1)
			for s in (separation + 1):
				$TileMap.set_cellv(source_cell + (vector * s), 0)
		else:
			$TileMap.set_cellv(path[c] * (separation + 1), 0)
