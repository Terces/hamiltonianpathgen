extends Node2D

onready var HamPathGen = load("res://HamiltonianPathGen.gd").new()

# helping variables
var bigger = PoolVector2Array()

func _ready():
	$CanvasLayer/Controls/mainCont/zoomSlider.connect("value_changed", self, "change_zoom")
	$CanvasLayer/Controls/mainCont/genButton.connect("pressed", self, "create_path")
	$CanvasLayer/Controls/mainCont/genButtonStrict.connect("pressed", self, "create_path_strict")
	$CanvasLayer/Controls/mainCont/sizeCont/xSize.connect("value_changed", self, "change_settings")
	$CanvasLayer/Controls/mainCont/sizeCont/ySize.connect("value_changed", self, "change_settings")
	$CanvasLayer/Controls/mainCont/startPosCont/startX.connect("value_changed", self, "change_settings")
	$CanvasLayer/Controls/mainCont/startPosCont/startY.connect("value_changed", self, "change_settings")
	$CanvasLayer/Controls/mainCont/endPosCont/endX.connect("value_changed", self, "change_settings")
	$CanvasLayer/Controls/mainCont/endPosCont/endY.connect("value_changed", self, "change_settings")
	$CanvasLayer/Controls/mainCont/fillRateMinCont/fillRatioMin.connect("value_changed", self, "change_settings")
	$CanvasLayer/Controls/mainCont/fillRateMaxCont/fillRatioMax.connect("value_changed", self, "change_settings")
	$CanvasLayer/Controls/mainCont/strictFill.connect("value_changed", self, "change_settings")

func create_path():
	var result = HamPathGen.generate_hamiltonian_path()
	bigger.resize(0)
	for location in result:
		bigger.append(location * 32)
	update()

func create_path_strict():
	var result = HamPathGen.generate_hamiltonian_path_strict()
	bigger.resize(0)
	for location in result:
		bigger.append(location * 32)
	update()

func _draw():
	if bigger.size() > 0:
		draw_polyline(bigger, Color(1, 0, 0, 1), 10)

func change_zoom(value):
	$Camera2D.zoom = Vector2(1, 1) * (10 / value)

func change_settings(value):
	HamPathGen.settings = {
		"xmax" : int($CanvasLayer/Controls/mainCont/sizeCont/xSize.value),
		"ymax" : int($CanvasLayer/Controls/mainCont/sizeCont/ySize.value),
		"start" : Vector2(int($CanvasLayer/Controls/mainCont/startPosCont/startX.value), int($CanvasLayer/Controls/mainCont/startPosCont/startY.value)),
		"end" : Vector2(int($CanvasLayer/Controls/mainCont/endPosCont/endX.value), int($CanvasLayer/Controls/mainCont/endPosCont/endY.value)),
		"fill_ratio_min" : $CanvasLayer/Controls/mainCont/fillRateMinCont/fillRatioMin.value,
		"fill_ratio_max" : $CanvasLayer/Controls/mainCont/fillRateMaxCont/fillRatioMax.value,
		"strict_limit" : $CanvasLayer/Controls/mainCont/strictFill.value,
		"avoid" : []
	}
	print("Changing settings to:", HamPathGen.settings)
